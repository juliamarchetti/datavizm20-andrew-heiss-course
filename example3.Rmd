---
title: "example3"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown for example of day 3

## Loading packages 
```{r load-packages, message=FALSE, warning=FALSE}
library(tidyverse)
library(readxl)
library(lubridate)
```



## Loading data 
```{r load data and rename columns, message=FALSE, warning=FALSE}
bbc_raw <- read_excel('data/360-giving-data.xlsx')
bbc <- bbc_raw %>% 
  rename(grant_amount = `Amount Awarded`, 
         grant_program = `Grant Programme:Title`, 
         grant_duration = `Planned Dates:Duration (months)`) %>%   mutate(grant_year = year(`Award Date`)) %>% 
  filter(grant_year > 2015) %>% 
  # crea una variable categorica
  mutate(grant_year_categorical=factor(grant_year))
```

## Plotting

# Histograms
```{r making-plots}
ggplot(data=bbc, mapping = aes(x = grant_amount))+
  geom_histogram(binwidth = 10000, color='white')

```

 veamos año y cantidad 
```{r histogram-year}

ggplot(data=bbc, mapping = aes(x = grant_amount, fill=grant_year_categorical))+
  geom_histogram(binwidth = 10000, color='white')
```

```{r histogram-year-facets}

ggplot(data=bbc, mapping = aes(x = grant_amount, fill=grant_year_categorical))+
  geom_histogram(binwidth = 10000, color='white')+
  facet_wrap(vars(grant_year_categorical))+
  #Elimina informacion en los labels
  guides(fill=FALSE)
```

# Points
```{r semi}
ggplot(bbc, aes(x=grant_year_categorical, y=grant_amount))+
  geom_point(alpha=0.1)
```


```{r change-dot-position}
ggplot(bbc, aes(x=grant_year_categorical, y=grant_amount, color=grant_year_categorical))+
  geom_point(position = position_jitter(height = 0, width=0.4), alpha= 0.2)+
 guides(color=FALSE)
```

```{r change-dot-position-grant-ammount}
ggplot(bbc, aes(x=grant_year_categorical, y=grant_amount, color=grant_program))+
  geom_point(position = position_jitter(height = 0, width=0.4), alpha= 0.2)
```

# Boxplot
```{r boxplot}
ggplot(bbc, aes(x=grant_year_categorical, y=grant_amount, color=grant_program))+
  geom_boxplot()+
 guides(color=FALSE)
```

# Summarized data 

```{r smaller-data-set}
bbc_by_year <- bbc %>% 
  group_by(grant_year_categorical) %>% 
  summarize(total_awarded=sum(grant_amount),
            avg_awarded = mean(grant_amount),
            number = n()) # n me dice la cantidad de filas
```

```{r smaller-data-set-col-plot}
ggplot(bbc_by_year, aes(x = grant_year_categorical, y = number))+
  geom_col()
```


```{r smaller-data-set-year-size}
bbc_by_year_size <- bbc %>% 
  group_by(grant_year_categorical,grant_program) %>% 
  summarize(total_awarded=sum(grant_amount),
            avg_awarded = mean(grant_amount),
            number = n()) # n me dice la cantidad de filas
```



```{r smaller-data-set-col-plot}
ggplot(bbc_by_year_size, aes(x = grant_year_categorical, y = number, fill= grant_program))+
  geom_col()
```

