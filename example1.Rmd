---
title: "example1"
output: html_document
---

# Introduction
- partes de una lista 

# Methodology

# Load and graph data
Cargamos la data de gapminder y luego filtramos seleccionando el año 1992

```{r load_library_data, message=FALSE, warning=FALSE}
library(tidyverse)
library(ggplot2)

gapminder <- read_csv('data/gapminder.csv')

gapminder_1992 <- gapminder %>% 
  filter(year == 1992)
```
# Hacemos un gráfico
```{r plot_data}
ggplot( data = gapminder_1992, 
         mapping = (aes (x = gdpPercap, y= lifeExp, color=continent)))+
  geom_point()
```

# Working with tibbles
```{r babynames}
library(babynames)
view(babynames)
```

```{r using filter function}
#select(babynames, name, sex)
filter(babynames, prop >= 0.08)
filter(babynames, name == "Khaleesi")
filter(babynames, is.na(n))

```
# Isolating data with dy
```{r using Filter and combining conditions}
filter(babynames, sex=='F' & name=='Sea')
filter(babynames, n %in% c(5,6) & year==1980)
```

```{r using Arrange function}
arrange(babynames, n)
arrange(babynames, year, desc(prop))

```
```{r using pipes}
babynames %>% 
  filter(name == 'Julia', sex == "F") %>% 
  ggplot( aes(x=year, y=prop))+ geom_point()

```

# Derive Information with dplyr
```{r getting the sum of the names}
babynames %>% 
  filter(name == "Julia", sex == "F") %>% 
    summarise(total = sum(n), max = max(n), mean = mean(n))

babynames %>% 
  filter(sex=='F') %>% 
  summarise(distintos_nombres=n_distinct(name), filas=n())

babynames %>% 
  group_by(year,name) %>% 
  summarise(total = sum(n))

```

```{r}
check <- babynames %>% 
  group_by(sex,name) %>% 
  summarise(total = sum(n)) %>% 
  arrange(desc(total)) %>% 
  ungroup() 
  
 # ggplot() +
  #  geom_line(aes(x = year, y = prop, color = name))
```

```{r mutate function}
babynames %>% 
  group_by(year) %>% 
  summarise(n = sum(n)) %>% 
  ggplot() +
    geom_line(aes(x = year, y = n))

```

```{r}
babynames %>% 
  mutate(births = n / prop)

# lo que sigue lo que hace es obtener la media de los rangos
# para nombres along different years
babynames %>% 
  group_by(year, sex) %>%
  mutate(rank = min_rank(desc(prop))) %>% 
  group_by(name, sex) %>% 
  summarise(score = median(rank)) %>% 
  arrange(score)
```
### How many distinct boys names acheived a rank of Number 1 in any year?

```{r Challenge}
babynames %>% 
  group_by(year, sex) %>% 
  mutate(rank = min_rank(desc(n))) %>% 
  filter(rank == 1, sex == "M") %>% 
  ungroup() %>% 
  summarise(distinct = n_distinct(name))
```

